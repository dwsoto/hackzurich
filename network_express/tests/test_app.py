import requests
import json


def test_add_preferences():
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

    payload = {'nearby_devices': ['2', '3', '4']}
    r = requests.post("http://0.0.0.0:5000/network_express/nearby_devices/1", data=json.dumps(payload), headers=headers)

    r = requests.get("http://0.0.0.0:5000/network_express/nearby_devices/2")
    print r.text

    num_matches = requests.get("http://0.0.0.0:5000/network_express/matches/1")
    print num_matches.text

    payload = {'preferences': ['animals', 'movies']}
    r = requests.post("http://0.0.0.0:5000/network_express/preferences/2", data=json.dumps(payload), headers=headers)

    payload = {'preferences': ['movies', 'energy drinks']}
    r = requests.post("http://0.0.0.0:5000/network_express/preferences/1", data=json.dumps(payload), headers=headers)

    payload = {'preferences': ['cockroaches', 'soft drinks']}
    r = requests.post("http://0.0.0.0:5000/network_express/preferences/3", data=json.dumps(payload), headers=headers)

    payload = {'preferences': ['cockroaches', 'energy drinks']}
    r = requests.post("http://0.0.0.0:5000/network_express/preferences/4", data=json.dumps(payload), headers=headers)

    num_matches = requests.get("http://0.0.0.0:5000/network_express/matches/1")
    print num_matches.text

    r = requests.get("http://0.0.0.0:5000/network_express/nearby_devices/1")
    print r.text

    payload = {'nearby_devices': ['2', '3', '4']}
    r = requests.post("http://0.0.0.0:5000/network_express/nearby_devices/1", data=json.dumps(payload), headers=headers)

    payload = {'nearby_devices': ['2', '3', '1']}
    r = requests.post("http://0.0.0.0:5000/network_express/nearby_devices/4", data=json.dumps(payload), headers=headers)

    payload = {'matches': [{"name": '1', "intent": "reject"}]}
    r = requests.post("http://0.0.0.0:5000/network_express/matches/4", data=json.dumps(payload), headers=headers)

    num_matches = requests.get("http://0.0.0.0:5000/network_express/matches/1")
    print num_matches.text

    num_matches = requests.get("http://0.0.0.0:5000/network_express/matches/4")
    print num_matches.text

if __name__ == "__main__":
    test_add_preferences()