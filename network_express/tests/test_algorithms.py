from algorithms import degree_of_match

def test_degree_of_match():
    assert degree_of_match(["Kittens", "Cows"], ["Kittens", "COwss"]) == 1
