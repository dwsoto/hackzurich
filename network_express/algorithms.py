def degree_of_match(array_strings_1, array_strings_2):
    set_string1 = set([x.strip().lower() for x in array_strings_1])
    set_string2 = set([x.strip().lower() for x in array_strings_2])
    return len(set_string1 & set_string2)


