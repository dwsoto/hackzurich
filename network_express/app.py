from flask import Flask, jsonify
from flask import abort
from flask import request
from algorithms import degree_of_match
app = Flask(__name__)
from flask import make_response
import operator
from collections import defaultdict

preferences_list= defaultdict(list)
nearby_devices = defaultdict(list)
intent_list = defaultdict(dict)
    
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.route('/network_express/nearby_devices/<id_phone>', methods=['GET'])
def get_nearby_devices(id_phone):
    response = []
    if id_phone in nearby_devices:
        response = nearby_devices[id_phone]
    return jsonify({'nearby_devices': response})

@app.route('/network_express/nearby_devices/<id_phone>', methods=['POST'])
def set_nearby_devices(id_phone):
    nearby_devices[id_phone] = request.json['nearby_devices']
    for id_ngh in nearby_devices[id_phone]:
        assert id_ngh != id_phone 
        nearby_devices[id_ngh].append(id_phone)

        nearby_devices[id_ngh] = list(set(nearby_devices[id_ngh]))

    return "200"

@app.route('/network_express/preferences/<id_phone>', methods=['GET'])
def get_interests(id_phone):
    return jsonify({'preferences': preferences_list[id_phone]})

@app.route('/network_express/preferences/<id_phone>', methods=['POST'])
def set_interests(id_phone):
    preferences_list[id_phone] = request.json['preferences']
    return "200"


def state_from_intent(id_1, id_2):
    state = "unknown"

    assert id_1 != id_2

    if id_1 in intent_list[id_2]:
        intent_1 = intent_list[id_2][id_1]["intent"]
    else:
        intent_1 = "unknown"

    if id_2 in intent_list[id_1]:
        intent_2 = intent_list[id_1][id_2]["intent"]
    else:
        intent_2 = "unknown"

    if intent_1 == "reject" or intent_2 == "reject":
        state = "reject"

    if intent_1 == "accept" and intent_2 == "accept":
        state = "accept"

    return state

@app.route('/network_express/matches/<id_phone>', methods=['GET'])
def get_matches(id_phone):
    response = []
    if id_phone in nearby_devices:
        for id_ngh in nearby_devices[id_phone]:
            if degree_of_match(preferences_list[id_phone], preferences_list[id_ngh]) > 0:
                state = state_from_intent(id_ngh, id_phone)
                response.append({"name": id_ngh, "state": state})

    return jsonify({'matches': response})

@app.route('/network_express/matches/<id_phone>', methods=['POST'])
def set_matches(id_phone):
    msg = request.json['matches']
    id_ngh = msg[0]["name"]

    if not (id_ngh in intent_list[id_phone]):
        intent_list[id_phone][id_ngh] = dict()
    intent_list[id_phone][id_ngh]["intent"] = msg[0]["intent"]

    return "200"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
