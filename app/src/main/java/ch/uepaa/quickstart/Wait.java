package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

public class Wait extends Activity {

    private View okButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wait);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        String otherID = getIntent().getStringExtra("name");
        okButton = findViewById(R.id.ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Wait.this, Profile.class);
                startActivity(i);
                finish();
            }
        });
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                RestClient.get("matches/" + P2PWrapper.getOwnNodeID(), null, new JsonHttpResponseHandler() {
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            JSONArray matches = response.getJSONArray("matches");
                            JSONObject firstMatch = matches.getJSONObject(0);
                            if (firstMatch.getString("state").equals("accept")) {
                                String otherID = firstMatch.getString("name");
                                Intent i = new Intent(Wait.this, SearchingFor.class);
                                i.putExtra("name", otherID.substring(0, otherID.indexOf("-") - 1));
                                timer.cancel();
                                timer.purge();
                                startActivity(i);
                                finish();
                            } else if (firstMatch.getString("state").equals("reject")) {
                                timer.cancel();
                                timer.purge();
                                Wait.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        okButton.setVisibility(View.VISIBLE);
                                        findViewById(R.id.sorry).setVisibility(View.VISIBLE);
                                    }
                                });

                            }
                        } catch (JSONException je) {
                            Log.e("TAG", je.toString());
                        }
                    }
                });
            }
        }, 0, 1000);
    }
}