package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Thanks extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thanks);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        String name = getIntent().getStringExtra("name");
        ((TextView)findViewById(R.id.otherName)).setText(name);
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Profile.class));
                finish();
            }
        });
    }
}