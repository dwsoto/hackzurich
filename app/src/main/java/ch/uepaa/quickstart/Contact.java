package ch.uepaa.quickstart;

/**
 * Created by david on 04.10.15.
 */
public class Contact {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
