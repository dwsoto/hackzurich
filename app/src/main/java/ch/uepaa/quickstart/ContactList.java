package ch.uepaa.quickstart;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by david on 04.10.15.
 */
public class ContactList {

    private static List<Contact> contacts = new LinkedList<Contact>();

    public static void addContact(Contact contact) {
        contacts.add(contact);
    }

    public static List<Contact> getContacts() {
        return contacts;
    }

}
