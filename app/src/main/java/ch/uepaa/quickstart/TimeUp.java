package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TimeUp extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeup);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = getIntent().getStringExtra("name");
                Contact newContact = new Contact();
                newContact.setName(name);
                ContactList.addContact(newContact);
                Intent intent = new Intent(getApplicationContext(), Thanks.class);
                intent.putExtra("name", name);
                startActivity(intent);
                finish();
            }
        });
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Profile.class));
                finish();
            }
        });
    }
}