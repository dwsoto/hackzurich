package ch.uepaa.quickstart;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by david on 03.10.15.
 */
public class PreferencesStore {

    private static List<Class> unsetInterestCategories = new LinkedList<Class>();

    private static List<String> interests = new LinkedList<String>();

    private static List<String> interestCategories = new LinkedList<String>();

    public static void addInterest(String interest) {
        interests.add(interest);
    }

    public static void addInterestCategory(String interestCategory) {
        interestCategories.add(interestCategory);
    }

    public static void addUnsetInterestCategory(Class clazz) {
        unsetInterestCategories.add(clazz);
    }

    public static Class getNextUnsetInterestCategory() {
        if (unsetInterestCategories.isEmpty()) {
            return null;
        }
        Class clazz = unsetInterestCategories.get(0);
        unsetInterestCategories.remove(0);
        return clazz;
    }

    public static void clearUnsetInterestCategories() {
        unsetInterestCategories.clear();
    }

    public static List<String> getInterests() {
        return new LinkedList<String>(interests);
    }

}
