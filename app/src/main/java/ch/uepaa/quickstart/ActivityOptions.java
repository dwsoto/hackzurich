package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;

public class ActivityOptions extends Activity {

    private ToggleButton fallInLove;

    private ToggleButton network;

    private ToggleButton learning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        if (!P2PWrapper.isConnected()) {
            P2PWrapper.setContext(this);
            P2PWrapper.enableKit();
        }
        fallInLove = ((ToggleButton)findViewById(R.id.fallInLove));
        network = ((ToggleButton)findViewById(R.id.network));
        learning = ((ToggleButton)findViewById(R.id.learning));
        fallInLove.setText("Fall in love");
        fallInLove.setTextOff("Fall in love");
        fallInLove.setTextOn("Fall in love");
        network.setText("Network");
        network.setTextOff("Network");
        network.setTextOn("Network");
        learning.setText("Learning");
        learning.setTextOff("Learning");
        learning.setTextOn("Learning");
        findViewById(R.id.arrow_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fallInLove.isChecked()) {
                    PreferencesStore.addInterestCategory("fallInLove");
                    PreferencesStore.addUnsetInterestCategory(LoveOptions.class);
                }
                if (network.isChecked()) {
                    PreferencesStore.addInterestCategory("network");
                }
                if (learning.isChecked()) {
                    PreferencesStore.addInterestCategory("learning");
                    PreferencesStore.addUnsetInterestCategory(LearningOptions.class);
                }
                Class nextCategory = PreferencesStore.getNextUnsetInterestCategory();
                if (nextCategory != null) {
                    Intent i = new Intent(getApplicationContext(), nextCategory);
                    startActivity(i);
                }
            }
        });
    }
}