package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Friends extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        TextView text = (TextView)findViewById(R.id.friendText);
        String friendString = "";
        for (Contact contact : ContactList.getContacts()) {
            friendString += contact.getName() + "\n";
        }
        text.setText(text.getText() + friendString);
    }
}