package ch.uepaa.quickstart;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import ch.uepaa.p2pkit.ConnectionCallbacks;
import ch.uepaa.p2pkit.ConnectionResult;
import ch.uepaa.p2pkit.ConnectionResultHandling;
import ch.uepaa.p2pkit.KitClient;
import ch.uepaa.p2pkit.discovery.P2pListener;
import ch.uepaa.p2pkit.discovery.Peer;
import ch.uepaa.p2pkit.discovery.PeersContract;
import cz.msebera.android.httpclient.Header;

public class P2PWrapper {

    public static boolean isConnected() {
        return connected;
    }

    private static final String APP_KEY = "eyJzaWduYXR1cmUiOiJRKzQ2MnpFQm5OR0V2Qm9LYjB1c0hhaG9H" +
            "aXlSV1lxWE5WQ0VneHlDTUlGUFJTeUl3UU5JWFFYNHJ5bVVyVk5MVjQ2UUlGdFJJVm1zQUk2c1g0cnBlN20x" +
            "SG5iQSttVFNXSFJKa2JKZHJsQXpiY3UxRzlJTUg5VUgxcFlmdnNsWGhUSTNaS2llOGY2S3dDZkYyME1sS" +
            "3ZUTEpSVG8xQzlkTmozdzVBbXBSL3c9IiwiYXBwSWQiOjEyODksInZhbGlkVW50aWwiOjE2ODAwLCJhcHBV" +
            "VVVJRCI6IjZCRjExRTY1LTlCMjMtNDc4Ny05NzUxLUJGNUEwQkZCQTVFMyJ9";

    private static boolean connected = false;

    private static Context context;

    private static boolean mShouldStartServices;

    private static boolean mWantToConnect;

    private static List<Peer> discoveredPeers = new LinkedList<Peer>();

    public static void setContext(Context context) {
        P2PWrapper.context = context;
    }

    // Initialization (1/2) - Connect to the P2P Services
    public static void enableKit() {

        final int statusCode = KitClient.isP2PServicesAvailable(context);
        if (statusCode == ConnectionResult.SUCCESS) {
            KitClient client = KitClient.getInstance(context);
            client.registerConnectionCallbacks(mConnectionCallbacks);

            if (!client.isConnected()) {
                client.connect(APP_KEY);
            }
            mWantToConnect = false;
        } else {
            mWantToConnect = true;
            Log.d("TAG", "Cannot start P2PKit, status code: " + statusCode);
            ConnectionResultHandling.showAlertDialogForConnectionError(context, statusCode);
        }
    }

    // Initialization (2/2) - Handle the connection status callbacks with the P2P Services
    private static final ConnectionCallbacks mConnectionCallbacks = new ConnectionCallbacks() {

        @Override
        public void onConnected() {
            connected = true;
            Log.d("TAG", "Successfully connected to P2P Services, with node id: " + KitClient.getInstance(context).getNodeId().toString());

                startP2pDiscovery();
        }

        @Override
        public void onConnectionSuspended() {
            Log.d("TAG", "Connection to P2P Services suspended");
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.d("TAG", "Connection to P2P Services failed with status: " + connectionResult.getStatusCode());
            ConnectionResultHandling.showAlertDialogForConnectionError(context, connectionResult.getStatusCode());
        }
    };

    public static void disableKit() {
        KitClient.getInstance(context).disconnect();
    }

    public static void startP2pDiscovery() {
        KitClient.getInstance(context).getDiscoveryServices().addListener(mP2pDiscoveryListener);
    }


    // Listener of P2P discovery events
    private static final P2pListener mP2pDiscoveryListener = new P2pListener() {

        @Override
        public void onStateChanged(final int state) {
            Log.d("TAG", "P2pListener | State changed: " + state);
        }

        @Override
        public void onPeerDiscovered(final Peer peer) {
            discoveredPeers.add(peer);
            JSONArray peerArray = new JSONArray();
            for (Peer currentPeer : discoveredPeers) {
                peerArray.put(peer.getNodeId());
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("nearby_devices", peerArray);
                RestClient.post(context, "nearby_devices/" + getOwnNodeID(), jsonObject, new JsonHttpResponseHandler() {
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.d("TAG", response.toString());
                    }
                });
                Uri peersContentUri = KitClient.getInstance(context).getPeerContentUri();
                ContentResolver contentResolver = context.getContentResolver();

                Cursor cursor = contentResolver.query(peersContentUri, null, null, null, null);

                int nodeIdColumnIndex = cursor.getColumnIndex(PeersContract.NODE_ID);
                int lastSeenColumnIndex = cursor.getColumnIndex(PeersContract.LAST_SEEN);

                while (cursor.moveToNext()) {
                    UUID nodeId = UUID.fromString(cursor.getString(nodeIdColumnIndex));
                    long lastSeen = cursor.getLong(lastSeenColumnIndex);

                    Log.d("TAG", "Peer: " + nodeId + " was last seen: " + SimpleDateFormat.getInstance().format(new Date(lastSeen)));
                }
                cursor.close();
            } catch (JSONException je) {
                Log.e("TAG", "Unable to create JSONObject");
            }
        }

        @Override
        public void onPeerLost(final Peer peer) {
            Log.d("TAG", "P2pListener | Peer lost: " + peer.getNodeId());
            discoveredPeers.remove(peer);
        }

        @Override
        public void onPeerUpdatedDiscoveryInfo(Peer peer) {
        }
    };

    public static void stopP2pDiscovery() {
        KitClient.getInstance(context).getDiscoveryServices().removeListener(mP2pDiscoveryListener);
        Log.d("TAG", "P2pListener removed");
    }

    public static String getOwnNodeID() {
        return KitClient.getInstance(context).getNodeId().toString();
    }

}
