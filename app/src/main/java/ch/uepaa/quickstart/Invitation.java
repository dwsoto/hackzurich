package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Invitation extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitation);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        try {
            final String otherID = getIntent().getStringExtra("name");
            final JSONObject object = new JSONObject();
            JSONArray matches = new JSONArray();
            JSONObject match = new JSONObject();
            match.put("name", otherID);
            //match.put("intent", "accept");
            matches.put(match);
            object.put("matches", matches);
            findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        object.getJSONArray("matches").getJSONObject(0).put("intent", "accept");
                        RestClient.post(Invitation.this, "matches/" + P2PWrapper.getOwnNodeID(), object, new JsonHttpResponseHandler() {

                        });
                        Intent i = new Intent(getApplicationContext(), Wait.class);
                        i.putExtra("name", otherID);
                        startActivity(i);
                        finish();
                    } catch (JSONException je) {

                    }
                }
            });
            findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        object.getJSONArray("matches").getJSONObject(0).put("intent", "reject");
                        RestClient.post(Invitation.this, "matches/" + P2PWrapper.getOwnNodeID(), object, new JsonHttpResponseHandler() {

                        });
                        startActivity(new Intent(getApplicationContext(), Profile.class));
                        finish();
                    } catch (JSONException je) {

                    }
                }
            });
        } catch (JSONException je) {

        }
    }
}