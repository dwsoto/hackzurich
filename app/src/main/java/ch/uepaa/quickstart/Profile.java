package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import ch.uepaa.p2pkit.KitClient;
import ch.uepaa.p2pkit.discovery.PeersContract;
import cz.msebera.android.httpclient.Header;

public class Profile extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray interestArray = new JSONArray();
            List<String> interests = PreferencesStore.getInterests();
            for (String interest : interests) {
                interestArray.put(interest);
            }
            jsonObject.put("preferences", interestArray);
            String nodeId = P2PWrapper.getOwnNodeID();
            RestClient.post(this, "preferences/" + nodeId, jsonObject, new JsonHttpResponseHandler() {
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d("TAG", response.toString());
                }
            });
        } catch (JSONException je) {
            Log.e("TAG", "Unable to create JSONObject");
        }
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                RestClient.get("matches/" + P2PWrapper.getOwnNodeID(), null, new JsonHttpResponseHandler() {
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            JSONArray matches = response.getJSONArray("matches");
                            JSONObject firstMatch = matches.getJSONObject(0);
                            if (firstMatch.getString("state").equals("unknown")) {
                                String otherID = firstMatch.getString("name");
                                Intent i = new Intent(Profile.this, Invitation.class);
                                i.putExtra("name", otherID);
                                timer.cancel();
                                timer.purge();
                                startActivity(i);
                                finish();
                            }
                        } catch (JSONException je) {
                            Log.e("TAG", je.toString());
                        }
                    }
                });
            }
        }, 0, 4000);
        findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                timer.purge();
                PreferencesStore.clearUnsetInterestCategories();
                Intent i = new Intent(getApplicationContext(), ActivityOptions.class);
                startActivity(i);
                finish();
            }
        });
        findViewById(R.id.friends).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                timer.purge();
                startActivity(new Intent(getApplicationContext(), Friends.class));
            }
        });

    }
}