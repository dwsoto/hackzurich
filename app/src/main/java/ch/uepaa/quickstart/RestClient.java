package ch.uepaa.quickstart;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by david on 03.10.15.
 */
public class RestClient {

    private static final String BASE_URL = "http://172.27.3.194:5000/network_express/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    private static SyncHttpClient syncClient = new SyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        syncClient.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, JSONObject jsonObject, AsyncHttpResponseHandler responseHandler) {
        try {
            StringEntity entity = new StringEntity(jsonObject.toString());
            client.post(context, getAbsoluteUrl(url), entity, "application/json",
                    responseHandler);
        } catch (UnsupportedEncodingException uee) {
            Log.e("TAG", "Failed to post");
        }
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
