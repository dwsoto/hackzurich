package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ToggleButton;

public class LearningOptions extends Activity {

    private ToggleButton languages;
    private ToggleButton stringTheory;
    private ToggleButton homeopathy;
    private ToggleButton blackHoles;
    private ToggleButton polarBears;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.learning_options);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        languages = ((ToggleButton) findViewById(R.id.languages));
        languages.setText("Languages");
        languages.setTextOff("Languages");
        languages.setTextOn("Languages");
        stringTheory = ((ToggleButton) findViewById(R.id.stringTheory));
        stringTheory.setText("String Theory");
        stringTheory.setTextOff("String Theory");
        stringTheory.setTextOn("String Theory");
        homeopathy = ((ToggleButton) findViewById(R.id.homeopathy));
        homeopathy.setText("Homeopathy");
        homeopathy.setTextOff("Homeopathy");
        homeopathy.setTextOn("Homeopathy");
        blackHoles = ((ToggleButton) findViewById(R.id.blackHoles));
        blackHoles.setText("Black Holes");
        blackHoles.setTextOff("Black Holes");
        blackHoles.setTextOn("Black Holes");
        polarBears = ((ToggleButton) findViewById(R.id.polarBears));
        polarBears.setText("Polar Bears");
        polarBears.setTextOff("Polar Bears");
        polarBears.setTextOn("Polar Bears");
        findViewById(R.id.arrow_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (languages.isChecked()) {
                    PreferencesStore.addInterest("languages");
                }
                if (stringTheory.isChecked()) {
                    PreferencesStore.addInterest("string theory");
                }
                if (homeopathy.isChecked()) {
                    PreferencesStore.addInterest("homeopathy");
                }
                if (blackHoles.isChecked()) {
                    PreferencesStore.addInterest("black holes");
                }
                if (polarBears.isChecked()) {
                    PreferencesStore.addInterest("polar bears");
                }
                Class nextInterestCategory = PreferencesStore.getNextUnsetInterestCategory();
                if (nextInterestCategory == null) {
                    nextInterestCategory = Profile.class;
                }
                Intent i = new Intent(getApplicationContext(), nextInterestCategory);
                startActivity(i);
            }
        });
        findViewById(R.id.arrow_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ActivityOptions.class);
                startActivity(i);
            }
        });
    }
}