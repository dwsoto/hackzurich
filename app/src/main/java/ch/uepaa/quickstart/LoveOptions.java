package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;

public class LoveOptions extends Activity {

    private ToggleButton biking;
    private ToggleButton chocolate;
    private ToggleButton homeopathy;
    private ToggleButton tulips;
    private ToggleButton greenEyes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.love_options);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        biking = ((ToggleButton) findViewById(R.id.biking));
        biking.setText("Biking");
        biking.setTextOff("Biking");
        biking.setTextOn("Biking");
        chocolate = ((ToggleButton) findViewById(R.id.chocolate));
        chocolate.setText("Chocolate");
        chocolate.setTextOff("Chocolate");
        chocolate.setTextOn("Chocolate");
        homeopathy = ((ToggleButton) findViewById(R.id.homeopathy));
        homeopathy.setText("Homeopathy");
        homeopathy.setTextOff("Homeopathy");
        homeopathy.setTextOn("Homeopathy");
        tulips = ((ToggleButton) findViewById(R.id.tulips));
        tulips.setText("Tulips");
        tulips.setTextOff("Tulips");
        tulips.setTextOn("Tulips");
        greenEyes = ((ToggleButton) findViewById(R.id.greenEyes));
        greenEyes.setText("Green Eyes");
        greenEyes.setTextOff("Green Eyes");
        greenEyes.setTextOn("Green Eyes");
        findViewById(R.id.arrow_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (biking.isChecked()) {
                    PreferencesStore.addInterest("biking");
                }
                if (chocolate.isChecked()) {
                    PreferencesStore.addInterest("chocolate");
                }
                if (homeopathy.isChecked()) {
                    PreferencesStore.addInterest("homeopathy");
                }
                if (tulips.isChecked()) {
                    PreferencesStore.addInterest("tulips");
                }
                if (greenEyes.isChecked()) {
                    PreferencesStore.addInterest("green eyes");
                }
                Class nextInterestCategory = PreferencesStore.getNextUnsetInterestCategory();
                if (nextInterestCategory == null) {
                    nextInterestCategory = Profile.class;
                }
                Intent i = new Intent(getApplicationContext(), nextInterestCategory);
                startActivity(i);
            }
        });
    }
}