package ch.uepaa.quickstart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;

public class SearchingFor extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searching_for);
        setupUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupUI() {
        ((TextView)findViewById(R.id.otherName)).setText(getIntent().getStringExtra("name"));
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){
                        TextView textView = (TextView) findViewById(R.id.timer);
                        int value = Integer.parseInt(textView.getText().toString()) - 1;
                        if (value == 0) {
                            Intent intent = new Intent(getApplicationContext(), TimeUp.class);
                            intent.putExtra("name", getIntent().getStringExtra("name"));
                            startActivity(intent);
                            finish();
                            timer.cancel();
                            timer.purge();
                        }
                        textView.setText(Integer.toString(value));
                    }
                });
            }
        }, 0, 1000);
    }
}